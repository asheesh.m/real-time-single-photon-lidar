clear all; close all;clc;
%%

load('plightwave_IRF.mat')


ordering = 'n';

fileID = fopen('datasets/IRF.irf','w');

% meta data
fwrite(fileID,size(IRF,3),'uint16',ordering);

% IRF values
for j=1:size(IRF,2)
    for i=1:size(IRF,1)
        for t=1:size(IRF,3)
            fwrite(fileID,IRF(i,j,t),'uint16',ordering);
        end
    end
end

fclose(fileID);